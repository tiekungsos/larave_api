<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');


Route::post('user/register', 'APIRegisterController@register');
Route::post('user/login', 'APILoginController@login');

Route::middleware('jwt.auth')->get('token', function(Request $request) {
    return auth()->user();
});

Route::middleware('jwt.auth')->get('users', function(Request $request) {
    return auth()->user();
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::post('user/logout', 'APILoginController@logout');
    Route::get('asset', 'API\AssetController@showAsset');
    Route::get('asset/{id}', 'API\AssetController@showAssetId');
    Route::get('book', 'API\AssetController@showAssetCondition');
    Route::post('asset', 'API\AssetController@addAsset');
    Route::put('asset/{id}', 'API\AssetController@updateAsset');
    Route::delete('asset/{id}', 'API\AssetController@removeAssetId');

    Route::post('asset/checkin/{id}', 'API\AssetController@checkinAsset');
    Route::post('asset/checkout/{id}/{user}', 'API\AssetController@checkoutAsset');
    
    Route::get('users', 'API\UserController@showUserList');
    Route::get('users/{id}', 'API\UserController@showUserId');
    Route::post('users', 'API\UserController@registerUser');
    Route::put('users/{id}','API\UserController@updateUser');
    Route::delete('users/{id}', 'API\UserController@removeUser');

    Route::get('categorie', 'API\AssetController@showAssetCategorie');
    Route::get('department', 'API\AssetController@showDepartment');
    Route::get('computer/{id}', 'API\AssetController@showComputer');
    Route::get('booking' ,'API\AssetBookController@bookList');
    Route::get('report/pdf/{id}', 'API\ReportController@pdfGen');
    Route::get('report/pdf/user/{id}', 'API\ReportController@pdfUserGen');
    Route::get('report/getfile', 'API\ReportController@getFile');
    Route::get('report/getfile/user', 'API\ReportController@getFileUser');

    Route::get('report/asset/{categorie}/{status}', 'API\ReportController@export');
    Route::get('report/user/{department}', 'API\ReportController@exportUser');
    Route::get('booking/book/receive/{id}', 'API\AssetBookController@receiveAsset');
    Route::get('booking/book/{id}', 'API\AssetBookController@book');
    Route::get('booking/extend/{id}/{extend}' ,'API\AssetBookController@extendBook');
    Route::post('booking', 'API\AssetBookController@addAssetBook');
    Route::get('booking/checkDate/{dateB}/{dateA}', 'API\AssetBookController@checkDateAssetBook');

    Route::get('booking/cancel/{id}' ,'API\AssetBookController@cancelBook');
    Route::get('booking/checkAsset/{id}' ,'API\AssetBookController@checkAssetDateToBook');
    Route::get('booking/checkDateSelect/{id}/{dateB}/{dateA}', 'API\AssetBookController@checkDateBeforeSelect');
});

Route::post('department', 'API\SettingController@addDepartment');
Route::get('department/{id}/{department}', 'API\SettingController@editDepartment');
Route::delete('department/{id}', 'API\SettingController@deleteDepartment');

Route::post('categorie', 'API\SettingController@addCategorie');
Route::put('categorie/{id}', 'API\SettingController@editCategorie');
Route::delete('categorie/{id}', 'API\SettingController@deleteCategorie');

Route::post('users/resetPassword', 'API\UserController@passwordReset');

// Route::get('report', 'API\ReportController@export');
// Route::middleware('jwt.auth')->get('asset/{id}', 'API\AssetController@showAssetId');




