<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/hello', function () {
    return view('hello');
});

Route::get('command', function () {
	/* php artisan migrate */
    \Artisan::call('asset:check_date_book');
    print_r('Success Sent Email');
});
Route::get('/send/email', 'MailTest@mail');


Route::get('report/asset/{categorie}/{status}', 'API\ReportController@export');
Route::get('report/user/{department}', 'API\ReportController@exportUser');

Route::get('report/getfile', 'API\ReportController@getFile');

Route::get('report/pdf/{id}', 'API\ReportController@pdfGen');
Route::get('report/pdf/user/{id}', 'API\ReportController@pdfUserGen');

Route::get('report/download/pdf/{file}', 'API\ReportController@downloadPDF');

Route::get('users/password/reset/{token}', 'API\UserController@showResetForm');

Route::get('users/password/new/{token}', 'API\UserController@showNewForm');

Route::get('users/password/reset', 'API\UserController@showForm');
Route::get('users/password/new', 'API\UserController@showFormNew');

Route::post('users/userResetPassword', 'API\UserController@userResetPassword');
Route::post('users/userNewPassword', 'API\UserController@userNewPassword');
