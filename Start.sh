#!/bin/sh

set -e

docker stop $(docker ps -a -q )
docker-compose up -d nginx workspace mariadb maildev  

DONE="Start laradock Docker"
echo $DONE