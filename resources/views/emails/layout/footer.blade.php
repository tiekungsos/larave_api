
          <!-- END CENTERED WHITE CONTAINER -->
        </div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

<div class="footer">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="content-block">
          <span class="apple-link">Copyright © 2018 Aware Corporation Limited. All Rights Reserved.</span>
          <br> Don't like these emails? <a href="http://localhost:3000/">Unsubscribe</a>.
        </td>
      </tr>
      <tr>
        <td class="content-block powered-by">
          Powered by <a href="http://htmlemail.io">HTMLemail</a>.
        </td>
      </tr>
    </table>
  </div>
  