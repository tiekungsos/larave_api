
         <!-- START HEADER -->
         
         @include('emails.layout.header')

         <!-- START CENTERED WHITE CONTAINER -->
         @if ($config == 1)
          
         @elseif ($config == 2)
           
        @elseif ($config == 3)
             
         @endif

         @switch($config)
         @case(1)
         <span class="preheader">Asset Book is successful.</span>
             @break
         @case(2)
         <span class="preheader">Cancel Book Asset is successful.</span>
             @break
         @case(3)
         <span class="preheader">Start Book Asset is successful.</span>
             @break
         @case(4)
         <span class="preheader">Expand Book Asset is successful.</span>
             @break   
         @default                            
     @endswitch

         <table role="presentation" class="main">

           <!-- START MAIN CONTENT AREA -->
           <tr>
             <td class="wrapper">
               
                         
                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                   <tr>
                   <td>
                    @switch($config)
                        @case(1)
                            <p>Hello, {{$user}} has successfully Book Asset.</p>
                            @break
                        @case(2)
                            <p>Hello, {{$user}} has successfully Cancel Book Asset.</p>
                            @break
                        @case(3)
                            <p>Hello, <strong>{{$admin}}</strong>  Asset that {{$user}} have booked up to date must go to pick up the equipment.
                                <strong>Please deliver the device to the {{$user}}.</strong> 
                            </p>
                            @break
                        @case(4)
                            <p>Hello, {{$admin}} device of {{$user}} that has been booked has expired. 
                                <strong>Please return the device from the user.</strong> 
                            </p>
                            @break
                        @case(6)
                            <p>Hello, {{$user}} You have booked the asset No. {{$asset->asset_no}}.
                                <strong>Please go to pick up the device that you have reserved at admin tomorrow</strong> 
                            </p>
                            @break
                        @case(7)
                            <p>Hello, {{$user}} Your device reservation has expired.
                                <strong>If you want to renew the reservation, press the button below to go to the website.</strong> 
                            </p>
                            @break
                        @case(8)
                            <p>Hello, {{$user}} Your device reservation has expired.
                                <strong>Please bring the device back to Admin tomorrow.</strong> 
                            </p>
                            @break 
                        @case(9)
                            <p>Hello, {{$user}} The system has already canceled the status from receive to be booked.
                            </p>
                            @break      
                        @default                            
                    @endswitch
                     <p>Book Asset information </p>
                     
                     <table>
                          <tr>
                             <td>User</td>
                             <td>{{$user}}</td>
                         </tr>
                           <tr>
                             <td>Book Start</td>
                             <td>{{ date('d-m-Y',strtotime($book->book_date)) }}</td>
                         </tr>
                         <tr>
                             <td>Book End</td>
                             <td>{{ date('d-m-Y',strtotime($book->book_end_date)) }}</td>
                         </tr>
                         <tr>
                             <td>Asset No</td>
                             <td>{{$asset->asset_no}}</td>
                         </tr>
                         <tr>
                             <td>Asset year</td>
                             <td>{{$asset->asset_year}}</td>
                         </tr>
                         <tr>
                             <td>Asset categorie</td>
                             <td>{{$asset->asset_categorie->value}}</td>
                         </tr>
                         <tr>
                             <td>Asset department</td>
                             <td>{{$asset->department->value}}</td>
                         </tr>
                         <tr>
                             <td>Asset Description</td>
                             <td>{{$asset->desc}}</td>
                         </tr>
                     </table>
                     
                     <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                       <tbody>
                         <tr>
                           <td align="center">
                             <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                               <tbody>
                                 <tr>
                                    @switch($config)
                                        @case(1)
                                        <td> <a href="{{$url}}" target="_blank">Check asset</a> </td>
                                            @break
                                        @case(2)
                                        <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                            @break
                                        @case(3)
                                        <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                            @break
                                        @case(4)
                                        <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                            @break 
                                        @case(6)
                                            <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                            @break
                                        @case(7)
                                            <td> <a href="{{$url}}" target="_blank">Extend Book Asset</a> </td>
                                            @break 
                                        @case(8)
                                            <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                            @break  
                                        @case(9)
                                            <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                            @break   
                                        @default                            
                                    @endswitch       
                                 </tr>
                               </tbody>
                             </table>
                           </td>
                         </tr>
                       </tbody>
                     </table>
                     <p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>
                     <p>Best regards, Asset management system.</p>
                   </td>
                 </tr>
               </table>
                 
             </td>
           </tr>

         <!-- END MAIN CONTENT AREA -->
         </table>

         <!-- START FOOTER -->
     @include('emails.layout.footer')
         <!-- END FOOTER -->
