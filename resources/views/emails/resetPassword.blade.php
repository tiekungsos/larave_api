
         <!-- START HEADER -->
         @include('emails.layout.header')

         <!-- START CENTERED WHITE CONTAINER -->
           <span class="preheader">Reset Password.</span>
         
         <table role="presentation" class="main">

           <!-- START MAIN CONTENT AREA -->
           <tr>
             <td class="wrapper">
               
                         
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            {{-- @foreach ($user as $user)   --}}
                        <tr>
                                
                            <td>
                                <p>Hello, You have reset password.</p> 
                     <br>
                     <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                       <tbody>
                         <tr>
                           <td align="center">
                             <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                               <tbody>
                                 <tr>
                                 <td> <a href="{{$url}}/users/password/reset/{{$token}}" target="_blank">Reset Password</a> </td>
                                 </tr>
                               </tbody>
                             </table>
                           </td>
                         </tr>
                       </tbody>
                     </table>
                     <p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>
                     <p>Best regards, Asset management system.</p>
                   </td>
                 </tr>
                 {{-- @endforeach --}}
               </table>
                 
             </td>
           </tr>

         <!-- END MAIN CONTENT AREA -->
         </table>

         <!-- START FOOTER -->
     @include('emails.layout.footer')
         <!-- END FOOTER -->
