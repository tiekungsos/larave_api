
         <!-- START HEADER -->
         @include('emails.layout.header')

         <!-- START CENTERED WHITE CONTAINER -->
           <span class="preheader">User registration is successful.</span>
         
         <table role="presentation" class="main">

           <!-- START MAIN CONTENT AREA -->
           <tr>
             <td class="wrapper">
               
                         
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            {{-- @foreach ($user as $user)   --}}
                        <tr>
                                
                            <td>
                                <p>Hello, {{$user->name}} has successfully registered the User.</p>
                              
                              <p>User information</p>
                           
                              <table role="presentation"  border="1" cellpadding="5" cellspacing="0">
                                  <tr>
                                      <td>Fullname</td>
                                      <td>{{$user->firstname}}&nbsp;{{$user->lastname}}</td>
                                  </tr>
                                  <tr>
                                      <td>Name</td>
                                      <td>{{$user->name}}</td>
                                  </tr>
                                  <tr>
                                      <td>Email</td>
                                      <td>{{$user->email}}</td>
                                  </tr>
                                  <tr>
                                      <td>User Type</td>
                                      <td>{{$user->user_type}}</td>
                                  </tr>
                              </table>
                             
                     <br>
                     <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                       <tbody>
                         <tr>
                           <td align="center">
                             <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                               <tbody>
                                 <tr>
                                     <td> <a href="{{$url}}/users/password/new/{{$user->token}}" target="_blank">Set New Password</a> </td>
                                 </tr>
                               </tbody>
                             </table>
                           </td>
                         </tr>
                       </tbody>
                     </table>
                     <p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>
                     <p>Best regards, Asset management system.</p>
                   </td>
                 </tr>
                 {{-- @endforeach --}}
               </table>
                 
             </td>
           </tr>

         <!-- END MAIN CONTENT AREA -->
         </table>

         <!-- START FOOTER -->
     @include('emails.layout.footer')
         <!-- END FOOTER -->
