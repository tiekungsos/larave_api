
         <!-- START HEADER -->
        @include('emails.layout.header')

            <!-- START CENTERED WHITE CONTAINER -->
            @if ($config == 1)
              <span class="preheader">Asset registration is successful.</span>
            @else
              <span class="preheader">Cancel Asset is successful.</span>
            @endif
            
            <table role="presentation" class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                  
                            
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                      <td>
                        @if ($config == 1)
                          <p>Hello, {{$user}} has successfully registered the Asset.</p>
                        @else
                          <p>Hello, {{$user}} has successfully Cancel the Asset.</p>
                        @endif
                        
                        <p>Asset information</p>
                        @foreach ($asset as $asset)  
                        <table>
                            <tr>
                                <td>Asset No</td>
                                <td>{{$asset->asset_no}}</td>
                            </tr>
                            <tr>
                                <td>Asset year</td>
                                <td>{{$asset->asset_year}}</td>
                            </tr>
                            <tr>
                                <td>Asset categorie</td>
                                <td>{{$asset->asset_categorie->value}}</td>
                            </tr>
                            <tr>
                                <td>Asset department</td>
                                <td>{{$asset->department->value}}</td>
                            </tr>
                            <tr>
                                <td>Asset Description</td>
                                <td>{{$asset->desc}}</td>
                            </tr>
                        </table>
                        @endforeach
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                          <tbody>
                            <tr>
                              <td align="left">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      @if  ($config == 1)
                                        <td> <a href="{{$url}}" target="_blank">Check asset</a> </td>
                                      @else
                                        <td> <a href="{{$url}}" target="_blank">Enter Website</a> </td>
                                      @endif          
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>
                        <p>Best regards, Asset management system.</p>
                      </td>
                    </tr>
                  </table>
                    
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

            <!-- START FOOTER -->
        @include('emails.layout.footer')
            <!-- END FOOTER -->
