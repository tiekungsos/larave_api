<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Asset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asset_no');
            $table->string('asset_year');
            $table->unsignedInteger('categorie_id');
            // $table->string('asset_categorie');
            $table->string('asset_name');
            $table->string('desc');
            $table->string('serial_no');
            $table->string('part_no');
            $table->string('asset_status');
            $table->unsignedInteger('department_id');
            // $table->string('asset_department');
            $table->string('comment');
            $table->timestamps();

            $table->foreign('categorie_id')->references('id')->on('asset_categorie');
            $table->foreign('department_id')->references('id')->on('department');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset');
    }
}
