<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssetCategorie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_categorie', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value', 100);
            $table->string('text', 100);
        });
    }
    // "id": 1,
    // "value": "Notebook",
    // "text": "Notebook"
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_categorie');
    }
}
