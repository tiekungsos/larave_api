<?php

namespace App\Mail;
use App\Asset; 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssetManagement extends Mailable
{
    use Queueable, SerializesModels;
    protected $asset;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $asset,
        $user,
        $config)
    {
        $this->asset = $asset;
        $this->user = $user;
        $this->config = $config; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->view('emails.asset')
            ->with([
                'asset' => $this->asset,
                'user' => $this->user,
                'config' => $this->config,
                'url' => 'http://localhost:3000/booking'
            ]);
    }
}
