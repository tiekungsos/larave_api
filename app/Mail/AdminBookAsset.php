<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminBookAsset extends Mailable
{
    use Queueable, SerializesModels;
    protected $asset;
    protected $user;
    protected $book;
    protected $config;
    protected $admin;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $asset,
        $user,
        $config,
        $book,
        $admin
        )
    {
        $this->asset = $asset;
        $this->user = $user;
        $this->config = $config; 
        $this->book = $book; 
        $this->admin = $admin;    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->view('emails.adminBook')
            ->with([
                'asset' => $this->asset,
                'user' => $this->user->name,
                'config' => $this->config,
                'book' => $this->book,
                'admin' => $this->admin,
                'url' => 'http://localhost:3000/booking'
            ]);
    }
}
