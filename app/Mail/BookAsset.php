<?php

namespace App\Mail;
use App\Asset; 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookAsset extends Mailable
{
    use Queueable, SerializesModels;
    protected $asset;
    protected $user;
    protected $book;
    protected $config;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $asset,
        $user,
        $config,
        $book
        )
    {
        $this->asset = $asset;
        $this->user = $user;
        $this->config = $config; 
        $this->book = $book; 
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->view('emails.book')
            ->with([
                'asset' => $this->asset,
                'user' => $this->user->name,
                'config' => $this->config,
                'book' => $this->book,
                'url' => 'http://localhost:3000/booking'
            ]);
    }
}
