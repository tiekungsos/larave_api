<?php

namespace App\Http\Controllers;
use App\Asset; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class MailTest extends Controller
{
    public function mail()
    {       
            $asset = Asset::get()->first(); 
            // dd($asset );
            $name = 'Krunal';
            Mail::to('e575630af1-be7422@inbox.mailtrap.io')->send(new SendMailable($asset));
            return 'Email was sent';
     }
}
