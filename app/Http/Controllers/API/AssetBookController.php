<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Http\Requests\AssetBookPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\AssetBook; 
use App\Asset; 
use App\User;
use App\AssetCategorie;
use Validator;
use JWTFactory;
use JWTAuth;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Mail;
use App\Mail\BookAsset;


class AssetBookController extends Controller
{
    public $successStatus = 200;


    private function setDataToBookList($assetList){
        $user = Auth::user(); 
        if($this->checkPermission() == false){
            $assetList =  AssetBook::with(['asset','user'])->where('user_id',$user->id)->get();
        }
       
        $i = 0;
        $assetAll = [];
        foreach ($assetList as $asset ) {
                $AssetCategorie =  Asset::with(['asset_categorie'])->find($asset->asset->id);
                $array = collect($asset);
                $asset = $array->merge(['categorie' => $AssetCategorie->asset_categorie->value]);
                $assetAll[$i] = $asset;
                $i++;
            }
        return $assetAll;
    }

    public function bookList(){
        $user = Auth::user(); 

        if($this->checkPermission()){
            $assetList = AssetBook::with(['asset','user'])->where('status','!=',3)->get();
        }else{
            $assetList = AssetBook::with([
            'asset',
            'user' => function($query) use ($user) {
                return $query->where('id', $user->id);
            },])
            ->get();   
            
        }

        $assetAll = $this->setDataToBookList($assetList);
        return response()->json($assetAll ,$this-> successStatus); 
    }

    public function book($id){
        $asset = AssetBook::with(['user'])->find($id); 
        return response()->json($asset ,$this-> successStatus); 
    }


    public function checkAssetDateToBook($id){
        $assetsDate = AssetBook::with(['user'])->where('asset_id',$id)->get()->toArray();
        $assetevent= [];
        $i = 0;
        $datelist=[];
                
        foreach ($assetsDate as $asset) {
            $dateEnd = new Carbon($asset['book_end_date']); 
            $dateEnd->addDay(1);
            $assetevent[$i]['title'] = $asset['user']['email'];
            $assetevent[$i]['start'] = $asset['book_date'];
            $assetevent[$i]['end']   = $dateEnd->toDateString() ;
            $datelist = array_prepend($datelist, $this->listDateOfDateToBook($asset['book_date'],$asset['book_end_date'])); 
            $i++;
        }
      
        $listDate = array_collapse($datelist);
        return response()->json(['event' => $assetevent,'datelist' => $listDate ],$this-> successStatus); 
    }

    public function checkDateBeforeSelect($id,$dateB,$dateA){
        $assetsBook = AssetBook::select('asset_id')
        ->where('asset_id',$id)
        ->whereBetween('book_date',array($dateB,$dateA))
        ->get()->toarray();
        
        $assetsBookEnd = AssetBook::select('asset_id')
        ->where('asset_id',$id)
        ->whereBetween('book_end_date',array($dateB,$dateA))
        ->get()->toarray(); 

        if($assetsBook || $assetsBookEnd){
            return response()->json('have asset', $this-> successStatus); 
             
        }
        // else{
        //     return response()->json('Not have Asset', $this-> successStatus);
        // }

    }

    private function listDateOfDateToBook($before,$after){
        $thisDates = null;
        $startTime = strtotime($before);
        $endTime = strtotime($after);
        $thisDate = [];
        for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {
                $thisDate[$i] = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc
                $thisDates[] = $thisDate[$i];
        }
        return $thisDates;
    }

    public function addAssetBook(AssetBookPost $request){
        $validated = $request->validated();     
        $thisDateCheck = $this->checkDateMuch($validated);
        if($thisDateCheck != null){
            return response()->json(['error' => 'Add Booking fail'], 404);
        }
        else{
            $assetBook = AssetBook::create($validated); 
            $assetBook->save();
            
            $asset = Asset::find($validated['asset_id']);
            $asset->status_id = 3;
            $asset->save();
            $config = 1;
            $this->SendEmail($assetBook,$config);
            return response()->json('Add Booking', $this-> successStatus); 
        }
    }

    public function checkDateAssetBook($dateB,$dateA){
        $assets = null;
        
        $assetsBook = AssetBook::select('asset_id')
                     ->whereBetween('book_date',array($dateB,$dateA))
                     ->get()->toarray();

        $assetsBookEnd = AssetBook::select('asset_id')
                     ->whereBetween('book_end_date',array($dateB,$dateA))
                     ->get()->toarray();             

        
        $assets = Asset::with(['asset_categorie','department','users','status'])
                 ->whereIn('status_id',[1,3,4,5])
                 ->whereNotIn('id',$assetsBook)
                 ->whereNotIn('id',$assetsBookEnd)
                 ->where('delete',0)
                 ->get()
                 ->toarray(); 
        if($assets){
            return response()->json(['asset' => $assets ],$this-> successStatus); 
        }
        
        return response()->json(['error' => 'No Asset'], 404);
        // return response()->json($assets, $this-> successStatus); 
    }

    public function cancelBook($id){
        //seach book asset
        // if($this->checkPermission()){
            $assetsBook = AssetBook::find($id);
            $config = 2;
            $this->SendEmail($assetsBook,$config);

            $assetDate = AssetBook::where('book_date','>',$assetsBook->book_date)
            ->where('asset_id',$assetsBook->asset_id)
            ->where('id','!=',$id)->get();
            $asset = Asset::find($assetsBook->asset_id);
            
            if($assetDate->isEmpty()){
                $asset->status_id = 1;
            }else{
                $asset->status_id = 3;
            }

            $asset->asset_user = null;
            $asset->save();


            $cancelBook = AssetBook::destroy($id);
            return response()->json(['Success Cancel Asset'],$this-> successStatus);
        // }
        // else{
        //     return response()->json(['You can not do this'],$this-> successStatus);
        // }
         
    }

    private function checkDateMuch($validated){
        $assets = AssetBook::where('asset_id',$validated['asset_id'])->get(); 
        $thisDates = null;
        foreach ($assets as $asset) {
            $startTime = strtotime($asset['book_date']);
            $endTime = strtotime($asset['book_end_date']);
            $thisDate = [];
            for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {
                $thisDate[$i] = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc
                if($thisDate[$i]>=$validated['book_date']){
                    if($thisDate[$i]<=$validated['book_end_date']){
                        $thisDates[] = $thisDate[$i];
                    }   
                }
                else{
                    $thisDates = null;
                }
            }   
        }
        return $thisDates;
    }

    public function extendBook($id,$extend){

        $asset = AssetBook::find($id);
        $asset->book_end_date = $extend;
        $asset->status = 1;
        $asset->save();

        return response()->json(['Success Extend Asset'],$this-> successStatus); 
    }

    public function receiveAsset($id){
        $book = AssetBook::find($id);
        $book->status = 3;
        $book->save();
        $findAsset = $this->getAssetBook($book->asset_id);

        $asset = Asset::find($book->asset_id);
        if($findAsset->isEmpty()){
            $asset->status_id = 1;
        }else{
            $asset->status_id = 3;
        }
        $asset->asset_user = null;
        $asset->save();
        
    
        return response()->json(['Success Receive Asset'],$this-> successStatus); 
    } 


    private function getAssetBook($book){
        $assetsBook = AssetBook::where('asset_id',$book)
        ->where('status','!=',3)
        ->where('status','!=',4)
        ->get();
        return $assetsBook;
    }
    
    public function SendEmail($book,$config){
        $asset = Asset::find($book->asset_id);
        $user = User::where('id',$book->user_id)->get();
        foreach ($user as $user) {
            Mail::to($user->email)->send(
                new BookAsset(
                    $asset,
                    $user,
                    $config,
                    $book                
            )); 
        }
        
    }

    private function checkPermission(){
        $user = Auth::user(); 
        if($user->user_type == 'admin'){
            return true;
        }else{
            return false;
        }
    }
}
