<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use App\Department;
use App\AssetCategorie;

class SettingController extends Controller
{ 
    public $successStatus = 200;

    public function addDepartment(Request $request){
        $department = new Department;
        // dd($request->Input);
        $department->value = $request->Input;
        $department->text = $request->Input;
        $department->timestamps = false;
        $department->save();

        return response()->json(['Add Department Success'], $this-> successStatus);
    }

    public function editDepartment($id,$departmentText){
        $idDepartment = (int)$id;
        $department = Department::find($idDepartment);
        $department->value = $departmentText;
        $department->text = $departmentText;
        $department->timestamps = false;
        $department->save();

        return response()->json(['Edit Department Success'], $this-> successStatus);
    }

    public function deleteDepartment($id){
        $idDepartment = (int)$id;
        $department = Department::find($idDepartment);
        $department->delete();
        return response()->json(['Edit Department Success'], $this-> successStatus);
    }

    public function deleteCategorie($id){
        $idCategorie = (int)$id;
        $Categorie = AssetCategorie::find($idCategorie);
        $Categorie->delete();
        return response()->json(['Edit Categorie Success'], $this-> successStatus);
    }

    public function editCategorie(Request $request,$id){
        $Categorie = AssetCategorie::find($id);
       
        if(!$request->computer){
            $Categorie->computer = false;
        }else{
            $Categorie->computer = true;
        }

        $Categorie->value = $request->categorieText;
        $Categorie->text = $request->categorieText;
        $Categorie->timestamps = false;
        $Categorie->save();

        return response()->json(['Edit Categorie Success'], $this-> successStatus);
    }


    public function addCategorie(Request $request){
        $categorie = new AssetCategorie;

        if($request->computer == 1){
            $categorie->computer = true;
        }else{
            $categorie->computer = false;
        }

        $categorie->value = $request->Input;
        $categorie->text = $request->Input;
        $categorie->timestamps = false;
        $categorie->save();

        return response()->json(['Add Categorie Success'], $this-> successStatus);
    }



}
