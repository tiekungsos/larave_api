<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Asset; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use Validator;
use application\json;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Requests\RegisterUser;
use Illuminate\Support\Facades\Mail;
use App\Mail\AssetManagementUser;
use Illuminate\Support\Facades\Hash;
use App\PasswordReset;
use Illuminate\Support\Str;
use App\Mail\PasswordResetMail;

class UserController extends Controller 
{
public $successStatus = 200;

public $url = 'http://localhost:3000';
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['meta' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function registerUser(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|string|email|max:255|unique:users', 
            // 'password' => 'required', 
            // 'c_password' => 'required|same:password' 
            'firstname' => 'required',
            'lastname' => 'required',
            'department_id' => 'required',
            'user_type' => 'required',]);
                
        if ($validator->fails()) {
                return response()->json($validator->errors());
            }

        $input = $request->all(); 
        $input['password'] = 'NotRegister'; 
        $user = User::create($input); 
        // $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
      

        $token = hash('sha256', Str::random(60));
        $user->token = $token;
        $password = New PasswordReset;
        $password->email = $user->email;
        $password->token = $token;
        $password->timestamps = false; 
        $password->save();

        $this->sendEmail($user);


        return response()->json($this-> successStatus);    
    }
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    {   
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    public function removeUser($id){
        
        $assets = User::with(['asset'])->where('id',$id)->get()->toArray(); 
        $index = 0;
        foreach ($assets as $asset) {
            $assetUser = $asset['asset'];
            foreach ($assetUser as $user) {
                $assetId[$index] =  $user['id'];
                DB::table('asset')
                ->where('id', $assetId[$index])
                ->update(['status_id' => 1,'asset_user' => null ,'updated_at' => Carbon::now()->format('Y-m-d')]);
                $index++;    
            }
        }

        $deleteAsset = User::find($id);
        $deleteAsset->asset()->detach();
        $deleteAsset->assetbook()->detach();
        $deleteAsset->delete();

        if($deleteAsset == 0){
            return response()->json(['error' => 'Resource not found'], 404);    
        }
        else{    
            return response()->json(['Asset Remove' => $deleteAsset], $this-> successStatus); 
        }
       
    }

    private function checkPermission(){
        $user = Auth::user(); 
        if($user->user_type == 'admin'){
            return true;
        }else{
            return false;
        }
    }

    public function showUserList(){

        $user = [];
        if($this->checkPermission()){
            $user = User::with(['department'])->get();
        }
        return response()->json($user, $this-> successStatus); 
    }

    public function showUserId($id){
        $users = Auth::user();
        if($this->checkPermission()){
            $user = User::with(['department','asset'])->where('id', $id)->get();
            return response()->json(['user' => $user], $this-> successStatus); 
        }else{
            $user = User::with(['department','asset'])->where('id', $users->id)->get();
            return response()->json(['user' => $user], $this-> successStatus); 
        }
        
    }

    public function updateUser(Request $request ,$id) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            // 'email' => 'required|string|email|max:255|unique:users', 
            // 'password' => 'required', 
            // 'c_password' => 'required|same:password' 
            'firstname' => 'required',
            'lastname' => 'required',
            'department_id' => 'required',
            'user_type' => 'required',]);
                
        if ($validator->fails()) {
                return response()->json($validator->errors());
        }
        $input = $request->all(); 
        $users = User::find($id);
        
        if($input['password_old'] != null && $input['password_new'] != null){
            if (Hash::check($input['password_old'], $users->password)){
                $input['password'] = bcrypt($input['password_new']); 
            }
            else{
                return response()->json(['error'=>'Please enter the correct password.'], 401);  
            }
        }

        $users->update($input); 
        return response()->json(['meta'=>$users], $this-> successStatus);    
    }

    public function logout(){
        auth()->logout();
    }

    public function sendEmail($user){  
            Mail::to($user->email)->send(new AssetManagementUser($user)); 
    }


    public function passwordReset(Request $request){
        $email = $request->email;
        $user = User::where('email',$email)->get();

        if($user->isNotEmpty()){
            $token = hash('sha256', Str::random(60));
            $password = New PasswordReset;
            $password->email = $email;
            $password->token = $token;
            $password->timestamps = false; 
            $password->save();

            Mail::to($email)->send(new PasswordResetMail($token)); 
            return response()->json(['Success'], $this-> successStatus); 
        }

        return response()->json(['Error'], $this-> successStatus);     
    }


    public function showResetForm($token){
        $password = PasswordReset::whereToken($token)->first();

        if($password){
            $user = User::whereEmail($password->email)->first();
            return view('resetPassword',compact('user'));
        }
        $url = $this-> url;
        $message = 'Link expires. Please reset the password again.';
        return view('errorToken',compact('url','message'));
    }

    public function showNewForm($token){
        $password = PasswordReset::whereToken($token)->first();

        if($password){
            $user = User::whereEmail($password->email)->first();
            return view('newPassword',compact('user'));
        }
        $url = $this-> url;
        $message = 'Link expires. Please reset the password again.';
        return view('errorToken',compact('url','message'));

    }

    public function userResetPassword(Request $request){

        $validator = Validator::make($request->all(), [ 
            'password' => 'required|confirmed'])->validate();
                
        $id = $request->id; 
        $password = $request->password;
        $users = User::find($id);

        $passwordIn= bcrypt($password); 
        $users->password = $passwordIn;
        $users->save();

       PasswordReset::where('email',$users->email)->delete();    
       $url = $this-> url;
        $message = 'Success Reset Password';
        return view('errorToken',compact('url','message'));
    }

    public function userNewPassword(Request $request){

        $validator = Validator::make($request->all(), [ 
            'password' => 'required|confirmed'])->validate();
                
        $id = $request->id; 
        $password = $request->password;
        $users = User::find($id);

        $passwordIn= bcrypt($password); 
        $users->password = $passwordIn;
        $users->save();

       PasswordReset::where('email',$users->email)->delete();    
       $url = $this-> url;
        $message = 'Success New Password';
        return view('errorToken',compact('url','message'));
    }



}