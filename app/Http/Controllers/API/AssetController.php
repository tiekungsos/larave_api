<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Asset; 
use App\AssetCategorie; 
use App\Department; 
use App\Computer; 
use App\Http\Requests\AssetPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Mail;
use App\Mail\AssetManagement;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Collection;

class AssetController extends Controller
{
    public $successStatus = 200;
    //
 
    private function checkPermission(){
        $user = Auth::user(); 
        if($user->user_type == 'admin'){
            return true;
        }else{
            return false;
        }
    }

    public function showAsset(){
        $user = Auth::user(); 
        if($this->checkPermission()){
            $assets = Asset::with(['asset_categorie','users','status','booking'])->where('delete',0)->get();   
        }else{
            $asset = Asset::with(['asset_categorie',
            'users' => function($query) use ($user) {
                return $query->where('id', $user->id);
            },
            'status',
            'booking'])->where('delete',0)->get(); 
            $assets = $this->setPermision($asset);
        }
        $assets->department = null;
        foreach ($assets as $asset) {
            if($asset->status->id != 1){
                $asset->department = $this->setDepartment($asset);
            }
            else{
                $asset->department = null;
            }
            if($asset->department == null){
                $asset->department = ['id'=> 0 ,'value'=>'','text'=>''];
              }
        }
        // dd($assets->toArray());
        return response()->json(['asset' => $assets ], $this-> successStatus); 
    }

    public function setDepartment($asset){
        foreach ($asset->users as $user) {
             $department = Department::find($user->department_id);
              return $department;
            } 
        }
        
    

    public function showAssetCondition(){
        $assets = Asset::with(['asset_categorie','department','users','status','booking'])->whereIn('status_id',[1,3,5])->where('delete',0)->get(); 
        return response()->json($assets, $this-> successStatus); 
    }

    public function showAssetId($id){

        $asset_users = Asset::with([
        'asset_categorie',
        'department',
        'usersCondition',
        'status'])
        ->where('id',$id)->get(); 


        $assets = Asset::with(['asset_categorie','department','users'])->where('id',$id)->get(); 
        if($assets == null){
            return response()->json(['error' => 'Resource not found'], 404); 
        }    
        else{
            return response()->json(['Asset_users' => $asset_users,'Asset' => $assets], $this-> successStatus); 
        }

    }
    
    public function addAsset(AssetPost $request){
        if($this->checkPermission()){
           try{
            $validated = $request->validated();
            $asset = Asset::create($validated); 

            $computer = new Computer([
                'cpu_name'      => $request->cpu_name,
                'harddisk_type' => $request->harddisk_type,
                'harddisk_qty'  => $request->harddisk_qty,
                'ram'           => $request->ram,
                'display'       => $request->display
            ]);

            $asset->computer()->save($computer);
            return response()->json(['Asset' => $asset], $this-> successStatus); 
           }
            catch (Exception $e){
                return response()->json($request['asset_no'], $this-> successStatus);   
            }
        }
        else{
            return response()->json(['You can not do this'], $this-> successStatus);
        }  
    }

    public function updateAsset(Request $request ,$id){
        // $validated = $request->validated();
        if($this->checkPermission()){
           try{
            $asset = Asset::find($id);
            $asset->asset_no        = $request['asset_no'];
            $asset->asset_year      = $request['asset_year'];
            $asset->categorie_id    = $request['categorie_id'];
            $asset->asset_name      = $request['asset_name'];
            $asset->desc            = $request['desc'];
            $asset->serial_no       = $request['serial_no'];
            $asset->part_no         = $request['part_no'];
            $asset->status_id    = $request['status_id'];
            $asset->department_id   = $request['department_id'];
            $asset->comment         = $request['comment'];
            $asset->save();
    
            $computer = Computer::where('asset_id', $id)->first();
            $computer->cpu_name        = $request['cpu_name'];
            $computer->harddisk_type   = $request['harddisk_type'];
            $computer->harddisk_qty    = $request['harddisk_qty'];
            $computer->ram             = $request['ram'];
            $computer->display         = $request['display'];    
            $computer->save();   
        
            return response()->json(['Asset' => $asset,'Computer' => $computer], $this-> successStatus);    
           }
           catch (Exception $e){
            return response()->json($request['asset_no'], $this-> successStatus);   
           }
        }
        else{
            return response()->json(['You can not do this'], $this-> successStatus);
        }
          
    }

    private function setAssetUser($id,$user){ 
        
        $user_asset = Asset::has('findUser')
        ->with(['findUser' => function($query) use ($user) {
            return $query->where('id', $user);
        }])
        ->where('id',$id)
        ->get()
        ->toArray(); 
        if($user_asset == null ){
            $username = null;
        }
        else{
            foreach ($user_asset as $asset) {
                if($asset['find_user'] != null){
                    $username = $asset['find_user'][0]['name'];     
                }    
                else {
                    $username = null;
                }     
            }
        }
       
        return $username;
    }
    
    public function checkinAsset(Request $request, $id){

        $user = $request['user']; 
        $username = $this->setAssetUser($id,$user);
            if($username == null){
                DB::table('asset_users')->insert(
                    ['asset_id' => $id, 'user_id' => $user , 'status' => 1 , 'created_at' => Carbon::now()->format('Y-m-d')]
                );
                $username_asset = $this->setAssetUser($id,$user);
                
                DB::table('asset')
                    ->where('id', $id)
                    ->update(['status_id' => 2,'asset_user' => $username_asset,'updated_at' => Carbon::now()->format('Y-m-d')]);
                // return response()->json(['Asset Checkin OK' => $user_asset], $this-> successStatus);
                // dd($username_asset);
                // exit;
            }    
            else{
                DB::table('asset_users')
                    ->where('asset_id',$id)
                    ->where('user_id',$user)
                    ->update(['status' => 1,'updated_at' => Carbon::now()->format('Y-m-d')]);  
                DB::table('asset')
                    ->where('id', $id)
                    ->update(['status_id' => 2,'asset_user' => $username,'updated_at' => Carbon::now()->format('Y-m-d')]);
                // return response()->json('Asset Checkin OK', $this-> successStatus);           
            }
        $config = 1;
        $this->sendEmail($id,$user,$config);        
        return response()->json(['Asset Checkin OK' => $username], $this-> successStatus);

    }

    public function checkoutAsset($id,$user){
             DB::table('asset_users')
                ->where('asset_id',$id)
                ->where('user_id',$user)
                ->update(['status' => 0]); 
             DB::table('asset')
                ->where('id', $id)
                ->update(['status_id' => 1,'asset_user' => null ,'updated_at' => Carbon::now()->format('Y-m-d')]);

                $config = 0;    
                $asset = $this->sendEmail($id,$user,$config);    
            return response()->json('Asset Checkout OK', $this-> successStatus);
      
    }

    public function removeAssetId($id){
        if($this->checkPermission()){
            $computer = Computer::where('asset_id', $id)->delete();
            $deleteAsset = Asset::find($id);
            
            $assetBook = Asset::with('booking')->where('id',$id)->get();

            foreach ($assetBook as $key => $value) {
                foreach($value->booking as $booking)
                if($booking){
                    $deleteBooking = app('App\Http\Controllers\API\AssetBookController')->cancelBook($booking->id);
                }
                
            }

            if(!$deleteAsset){
                return response()->json(['error' => 'Resource not found'], 404);    
            }
            else{    
                $deleteAsset->delete = 1 ;
                $deleteAsset->status_id = 4 ;
                $deleteAsset->save();
                return response()->json(['Asset Remove' => $deleteAsset], $this-> successStatus); 
            } 
        }
        else{
            return response()->json(['You can not do this'], $this-> successStatus);
        }  

        
       
    }

    public function showAssetCategorie(){
        $categorie = AssetCategorie::get();
        return response()->json($categorie, $this-> successStatus); 
    }

    public function showDepartment(){
        $department = Department::get();
        return response()->json($department, $this-> successStatus); 
    }

    public function showComputer($id){
        $computer = Computer::where('asset_id', $id)->get();
        return response()->json($computer, $this-> successStatus); 
    }

    public function sendEmail($assetId,$user,$config){
        $asset= Asset::with([
            'asset_categorie',
            'department'])
            ->where('id',$assetId)->get(); 
        $userData = User::where('id',$user)->get();    

        foreach ($userData as $user_data) {
            Mail::to($user_data->email)->send(
                new AssetManagement(
                    $asset,
                    $user_data->name,
                    $config
            )); 
        }   
    }

    private function setPermision($asset){
        
        $i=0;
        $user_asset = [];
        $asset_id = [];
        foreach ($asset as $asset) {
            $user = $asset->users;
            if($user->isNotEmpty()){
                $user_asset[$i] = $asset;
                $asset_id[$i] = $asset->id;
                $i++;
            }
        }
        $asset_list[] = Asset::with(['asset_categorie','department','users','status','booking'])
        ->where('status_id','!=',2)
        ->whereNotIn('id',$asset_id)
        ->where('delete',0)
        ->get();
        $collection = collect($asset_list);
        $collection->prepend($user_asset);
        $collection->all();

        $collapsed = $collection->collapse();
        $collapsed->all();
        
        return $collapsed;
    }

}
