<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Asset; 
use App\AssetCategorie; 
use App\Department; 
use App\Computer; 
use App\Http\Requests\AssetPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Exports\AssetExport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\Snappy\Facades\SnappyPdf;
class ReportController extends Controller
{
    public $successStatus = 200;
    

    public function export($categorie,$status) 
    {
        return (new AssetExport($categorie,$status))->download('asset.xlsx');
    }

    public function exportUser($department){
        return (new UsersExport($department))->download('user.xlsx');
    }

   public function getFile(){
       $filename = 'import_Asset.xlsx';
       return response()->download(storage_path("app/public/{$filename}"));
   }

   public function getFileUser(){
    $filename = 'import_User.xlsx';
    return response()->download(storage_path("app/public/{$filename}"));
   }
    public function pdfGen($id){
        $assets = Asset::with(['asset_categorie','department','users'])->where('id',$id)->get(); 
        $pdf = SnappyPdf::loadView('assetPdf',compact('assets'))->setOption('encoding', 'utf-8');
        $filename = 'pdf/Asset_'.$id.'.pdf';
        Storage::put($filename, $pdf->inline());
        $part = url("report/download/{$filename}");
        return $part;
    }

    public function pdfUserGen($id){
        $user = User::with(['department','asset'])->where('id', $id)->get();
        $pdf = SnappyPdf::loadView('userPdf',compact('user'))->setOption('encoding', 'utf-8');
        $filename = 'pdf/User_'.$id.'.pdf';
        Storage::put($filename, $pdf->inline());
        $part = url("report/download/{$filename}");
        return $part;
    }

    public function downloadPDF($file){
        $filename = $file;
        $part = storage_path("app/pdf/{$filename}");
        return response()->download($part);
    }
}
