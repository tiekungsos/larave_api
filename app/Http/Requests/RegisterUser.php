<?php

namespace App\Http\Requests;
use Validator;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required', 
            'email' => 'required|string|email|max:255|unique:users', 
            'password' => 'required', 
            // 'c_password' => 'required|same:password' 
            'firstname' => 'required',
            'lastname' => 'required',
            'department_id' => 'required',
            'user_type' => 'required',
        ];
    }
    
}
