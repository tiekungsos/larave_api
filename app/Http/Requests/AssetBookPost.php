<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssetBookPost extends FormRequest
{
    /**
     * Determine if the user is authorizexyyd to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_id'      => 'required', 
            'user_id'    => 'required', 
            'book_date'  => 'required', 
            'book_end_date'    => 'required', 
            'status'          => 'required'
        ];
    }


    public function messages()
    {
        return [
            
            'asset_id.required'     => 'Asset no is required!',
            'user_id.required'   => 'User no is required!',
            'book_date.required' => 'Book Date  is required!',
            'book_end_date.required'   => 'Book End Date is required!',
            'status.required'         => 'Status is required!',
        ];
    }
}
