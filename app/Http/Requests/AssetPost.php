<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssetPost extends FormRequest
{
    /**
     * Determine if the user is authorizexyyd to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_no'      => 'required', 
            'asset_year'    => 'required', 
            'categorie_id'  => 'required', 
            'asset_name'    => 'required', 
            'desc'          => 'required', 
            'serial_no'     => 'required', 
            'part_no'       => 'required', 
            // 'asset_status'  => 'required', 
            // 'department_id' => 'required', 
            // 'comment'       => 'required',  
            // 'users_id'      => 'numeric',
            // 'cpu_name'      => 'required',
            // 'harddisk_type' => 'required',
            // 'harddisk_qty'  => 'required',
            // 'ram'           => 'required',
            // 'display'       => 'required'  
        ];
    }


    public function messages()
    {
        return [
            
            'asset_no.required'     => 'Asset no is required!',
            'asset_year.required'   => 'Asset year is required!',
            'categorie_id.required' => 'Categorie d is required!',
            'asset_name.required'   => 'Asset Name is required!',
            'desc.required'         => 'Description is required!',
            'serial_no.required'    => 'Serial No is required!',
            'part_no.required'      => 'Part No is required!',
            'asset_status.required' => 'Status is required!',
            'department_id.required'=> 'Department is required!',
            // 'comment.required'      => 'Comment is required!',
            // 'users_id.numeric'      => 'Please Enter',
            // 'cpu_name.required'     => 'CPU Name is required!',
            // 'harddisk_type.required'=> 'Harddisk Type is required',
            // 'harddisk_qty.required' => 'Harddisk QTY is required',
            // 'ram.required'          => 'Ram is required',
            // 'display.required'      => 'Display is required' 
        ];
    }
}
