<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetCategorie extends Model
{
    //
    protected $table = 'asset_categorie';
    protected $fillable = ['value', 'text'];

}
