<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'asset';

    protected $fillable = [
        'asset_no', 'asset_year', 'categorie_id',  'asset_name',  'desc',  'serial_no',  'part_no',  'asset_status',
        'department_id','comment','users_id','updated_at', 'created_at' , 'delete'
    ];
    //
    public function asset_categorie()
    {
        return $this->belongsTo('App\AssetCategorie','categorie_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }  

    public function computer()
    {
        return $this->hasOne('App\Computer');
    }

    public function booking()
    {
        return $this->hasMany('App\AssetBook');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }  


    public function users()
    {
        return $this->belongsToMany('App\User','asset_users')->withPivot('status')->withTimestamps();
    }

    public function usersCondition()
    {
        return ($this->belongsToMany('App\User','asset_users')->withPivot('status')->wherePivot('status',1));
    }

    public function findUser()
    {
        return $this->belongsToMany('App\User','asset_users')->withPivot('status');
    }



}
