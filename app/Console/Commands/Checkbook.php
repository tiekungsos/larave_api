<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AssetBook;
use App\User;
use App\Asset;
use Illuminate\Support\Facades\Mail;
use App\Mail\BookAsset;
use App\Mail\AdminBookAsset;
use Carbon\Carbon;
class CheckBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:check_date_book';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email Asset Book Check date to day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $asset_book = AssetBook::where('status','!=',3)->get();
        
        foreach ($asset_book as $book) {
            $this->checkBookToDay($book);
            $this->checkBookToBefor($book);     
        }        
    }

    public function checkBookToDay($book){
            $config = null;
            $user = User::find($book->user_id);
            $asset = Asset::find($book->asset_id);
            $todayDate = date("Y-m-d");
            if($book->book_date == $todayDate){
                //set new status to asset
                $asset->status_id = 5;
                $asset->asset_user = $user->name;
                $asset->save();
                //config = 3 => start to book
                $config = 3;
            }  
            else if($book->book_end_date == $todayDate){
                //set new status to asset
                $assetsDate = AssetBook::where('asset_id',$book->asset_id)->count();
                if($assetsDate >= 0){
                    //status Booking
                    //config = 4 => Email Expand Book  ที่แจ้งตือนเมื่ออุปกรณ์มีคนจองเพื่อส่งคืน
                    $config = $this->checkUserBookNextDay($asset->id,$book->id);                    
                }
                // else{
                //     //status Available
                //     //config = 5 => Email Expand Book  ที่แจ้งตือนเพื่อส่งคืน
                //     $config = 5;
                //     $asset->status_id = 1;
                //     $asset->save();
                // } 
            }else if(carbon::parse($book->book_end_date)->addDay(3) == Carbon::today()){
                //Check asset book end   
                $config  = $this->receiveAssetAuto($book);
            }else if(carbon::parse($book->book_end_date)->addDay(3) < Carbon::today()){
                $this->receiveAssetAuto($book);
            }   

         

         //Check config if = 6 คือ Asset มีอยู๋ในฐานข้อมูลและยังคงใช้งานต่อไป  
        if($config != 5 && $config != null){

                if($config == 4 || $config == 3 || $config == 9){
                    $User = User::where('user_type','admin')->get();
                    foreach ($User as $admin) {
                        Mail::to($admin->email)->send(
                            new AdminBookAsset(
                                $asset,
                                $user,
                                $config,
                                $book,
                                $admin->name        
                        )); 
                        $this->info('Email Asset Book Check date to day has been send successfully');
                    }
                }

                Mail::to($user->email)->send(
                    new BookAsset(
                        $asset,
                        $user,
                        $config,
                        $book         
                )); 
                $this->info('Email Asset Book Check date to day has been send successfully');
            }


    }

    public function sentEmailAdmin(){

    }

    public function checkBookToBefor($book){
            $config = null;
            $user = User::find($book->user_id);
            $asset = Asset::find($book->asset_id);
            $nextDate = Carbon::now()->addDay(1)->format('Y-m-d');

            if($nextDate == $book->book_date){
                $config = 6;
            }  
            else if($nextDate == $book->book_end_date){
                //set new status to asset
                $config = $this->checkUserBookNextDayTo($asset->id,$book->id);                      
            }   

        //  Check config if = 6 คือ Asset มีอยู๋ในฐานข้อมูลและยังคงใช้งานต่อไป  
        if($config != null){
                Mail::to($user->email)->send(
                    new BookAsset(
                        $asset,
                        $user,
                        $config,
                        $book         
                )); 
                $this->info('Email Asset Book Check date to day has been send successfully');
            }
    }

    public function checkUserBookNextDay($id,$book){
        $datetoday = Carbon::now()->format('Y-m-d');
        $addDate = Carbon::now()->addDay(2)->format('Y-m-d');
        $assetsBook = $this->getAssetBook($id,$datetoday,$addDate);
       if(empty($assetsBook)){
            $config = 4; 
            $asset = AssetBook::find($book);
            $asset->status = 4; //หมดอายุแล้ว มีปุ่มให้ Admin กดรับของคืน 
            $asset->save();           
        }else{
            $config = 5;
        }
        return $config;            
    }

    public function checkUserBookNextDayTo($id,$book){
        $datetonextday = Carbon::now()->addDay(1);
        $onextday = Carbon::now()->addDay(1)->format('Y-m-d');
        $addDate = $datetonextday->addDay(3)->format('Y-m-d');
        $assetsBook = $this->getAssetBook($id,$onextday,$addDate);
       if(empty($assetsBook)){
            $asset = AssetBook::find($book);
            $asset->status = 2; //กำลังหมดอายุการใช้งาน
            $asset->save();
            $config = 7;           
        }else{
            $config = 8;
        }
        return $config;            
    }

    private function getAssetBook($id,$dayB,$dayA){

        $assetsBook = AssetBook::where('asset_id',$id)
        ->whereBetween('book_date',array($dayB,$dayA))
        ->get()->toarray();
        return $assetsBook;
    }

    public function receiveAssetAuto($booked){
        $book = AssetBook::find($booked->id);
            $book->status = 3;
            $book->save();
            $findAsset = $this->AssetBook($book->asset_id);

            $asset = Asset::find($book->asset_id);
            if($findAsset->isEmpty()){
                $asset->status_id = 1;
            }else{
                $asset->status_id = 3;
            }
            $asset->asset_user = null;
            $asset->save();
            $config = 9;
            return $config;   
    } 


    private function AssetBook($book){
        $assetsBook = AssetBook::where('asset_id',$book)
        ->where('status','!=',3)
        ->where('status','!=',4)
        ->get();
        return $assetsBook;
    }
}
