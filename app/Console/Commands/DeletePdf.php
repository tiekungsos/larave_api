<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DeletePdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:delete_pdf_file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete PDF file in store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileAll = Storage::allFiles('pdf');
        foreach ($fileAll as $file) {
           $delete = Storage::delete($file);
        }
        return $delete;
    }
}
