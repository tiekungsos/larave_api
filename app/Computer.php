<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Computer extends Model
{
    protected $fillable = [
        'cpu_name', 'harddisk_type', 'harddisk_qty','ram',  'display' , 'updated_at', 'created_at'
    ];

    public function asset()
    {
        return $this->hasOne('App\Asset');
    }

    //
    protected $table = 'computer';
}
