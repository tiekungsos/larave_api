<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromQuery,WithHeadings,WithMapping
{
    use Exportable;

    public function __construct($department)
    {
        $this->department = $department;
        return  $this;
    }

    public function headings(): array
    {
        return [
            'Firstname',
            'Lastname',
            'Name',
            'Department',
            'Email',
            'User Type'
        ];
    }

    public function query()
    {
        if($this->department == 0){
            $user = User::with(['department']);
        }else{
            $user = User::with(['department'])->where('department_id',$this->department);
        }

        // dd($assets);
        return $user;
    }


    public function map($user): array
    {
        return [
            $user->firstname,
            $user->lastname,
            $user->name,
            $user->department->text,
            $user->email,
            $user->user_type,
        ];
    }

}
