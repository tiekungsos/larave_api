<?php

namespace App\Exports;

use App\Asset;
use App\AssetCategorie;
use App\Status;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AssetExport implements FromQuery,WithHeadings,WithMapping
{
    use Exportable;

    public function __construct($categorie,$status)
    {
        $this->status = $status;
        $this->categorie = $categorie;
        return  $this;
    }

    public function headings(): array
    {
        return [
            'Asset No.',
            'Asset Year',
            'Asset Name',
            'Serial No.',
            'Part No.',
            'Description',
            'Comment',
            'Status',
            'Categorie'
        ];
    }

    public function query()
    {
        if ($this->categorie == 0 && $this->status == 0) {
            $assets = Asset::with(['asset_categorie','status']);
        }elseif($this->categorie == 0){
            $assets = Asset::with(['asset_categorie','status'])
            ->where('status_id',$this->status);
        }elseif($this->status == 0) {
            $assets = Asset::with(['asset_categorie','status'])
            ->where('categorie_id',$this->categorie);
        }else {
            $assets = Asset::with(['asset_categorie','status'])
            ->where('categorie_id',$this->categorie)
            ->where('status_id',$this->status);
        }
        return $assets;
    }

    public function map($assets): array
    {
        return [
            $assets->asset_no,
            $assets->asset_year,
            $assets->asset_name,
            $assets->serial_no,
            $assets->part_no,
            $assets->desc,
            $assets->comment,
            $assets->status->name,
            $assets->asset_categorie->text
        ];
    }

}
