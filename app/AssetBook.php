<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class AssetBook extends Model
{
    //

    protected $table = 'asset_book';

    protected $fillable = [
        'asset_id', 'user_id', 'book_date',  'book_start' ,'book_end_date',  'status',  'created_at',  'updated_at'
    ];

    public function asset()
    { 
        return $this->belongsTo('App\Asset','asset_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

   



}
