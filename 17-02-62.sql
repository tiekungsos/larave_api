-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2019 at 07:28 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categorie_id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED DEFAULT '11',
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `asset_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `part_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_status` int(2) NOT NULL DEFAULT '1',
  `asset_user` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`id`, `asset_no`, `asset_year`, `categorie_id`, `department_id`, `status_id`, `asset_name`, `desc`, `serial_no`, `part_no`, `asset_status`, `asset_user`, `comment`, `created_at`, `updated_at`) VALUES
(35, 'NB012_12/15', '2012', 2, 8, 2, 'Thinkpad Edge E420', 'อุปกรณ์หน้าจอ', 'R9ERK4T', 'LNV-11412ET', 0, 'demo', 'สวัสดี', '2018-10-09 09:33:56', '2019-01-29 17:00:00'),
(39, 'NB012_12/15', '2012', 1, 1, 2, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch', 'R9ERK4T', 'LNV-11412ET', 0, 'demo', 'uuuu', '2018-10-16 07:00:13', '2018-12-24 00:00:00'),
(40, 'NB012_12/15', '2012', 1, 2, 2, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch', 'R9ERK4T', 'LNV-11412ET', 0, 'demo', 'iiiii', '2018-10-16 07:02:42', '2019-01-14 00:00:00'),
(42, 'NB012_12/15', '2012', 1, 2, 2, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch', 'R9ERK4T', 'LNV-11412ET', 0, 'tiekungsos', 'ttt', '2018-10-16 07:05:06', '2019-01-02 00:00:00'),
(44, 'NB012_12/15', '2012', 1, 2, 5, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch', 'R9ERK4T', 'LNV-11412ET', 0, 'may', 'tttt', '2018-10-18 04:01:48', '2019-02-07 01:04:13'),
(54, 'NB012_12/15', '2018', 2, 4, 3, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 13 inch', 'R9ERK4T', 'oooooo', 0, NULL, 'ooooo', '2018-10-18 04:20:12', '2019-02-07 00:50:02'),
(56, 'NB012_12/15', '2012', 1, 1, 5, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 15 inch', 'R9ERK4T', 'LNV-11412ET', 0, 'tiekungsos', 'uuu', '2018-10-18 04:21:49', '2018-12-25 10:27:12'),
(57, 'NB012_12/15', '2012', 1, 1, 2, 'Thinkpad Edge E420', 'CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 15 inch', 'R9ERK4T', 'LNV-11412ET', 1, 'demo', 'tertert', '2018-11-05 02:03:40', '2019-01-29 17:00:00'),
(58, 'NB012_12/15', '2012', 1, 3, 2, 'Thinkpad Edge E420', 'CPU Intel Core i5, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch', 'R9ERK4T', 'LNV-11412ET', 1, 'demo', 'ooooo', '2018-11-06 05:00:00', '2019-02-14 17:00:00'),
(59, 'NB012_12/15', '2012', 2, 2, 5, 'Thinkpad Edge E420', 'อุปกรณ์หน้าจอ', 'R9ERK4T', 'LNV-11412ET', 1, 'demo', 'dasdas', '2018-11-06 06:07:21', '2019-02-07 00:52:36'),
(60, 'NB012_12/15', '2015', 2, 2, 3, 'Thinkpad Edge E420', 'addas', 'R9ERK4T', 'LNV-11412ET', 1, NULL, 'asdsad', '2018-11-06 06:08:28', '2019-01-31 10:10:23'),
(61, 'NB012_12/1', '2013', 1, 3, 2, 'Thinkpad Edge E420', 'CPU Intel Core i5, Harddisk HDD 512GB ,RAM 8GB ,Display 15 inch', 'R10ERK4T', 'LNV-11412ET', 1, 'demo', 'ปุ่มพัง22', '2018-11-06 06:13:15', '2019-02-12 17:00:00'),
(62, 'ererrwt', '2015', 1, 1, 1, 'sadfsdaf', 'fasadfa', 'sdafsad', 'sdafsadf', 1, NULL, 'fgfdsgdg', '2019-02-15 01:46:40', '2019-02-15 01:46:40'),
(64, 'gfg', '2015', 1, 1, 1, 'gdsfgdsf', 'dfsgsdfgsdfgd', 'sdfg', 'dsfgdf', 1, NULL, 'dsfgsfd', '2019-02-15 01:48:45', '2019-02-15 01:48:45'),
(65, 'hjh', '2015', 1, 11, 2, 'dsfgdsfg', 'sdgsf', 'dsfgdsfg', 'sdfgs', 1, 'demo', 'dsfgdsfgsdfg', '2019-02-15 01:50:53', '2019-02-14 17:00:00'),
(66, 'NB012_12/15', '2012', 1, 11, 1, 'Thinkpad Edge E420', 'อุปกรณ์หน้าจอ', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 04:05:06', '2019-02-15 04:05:06'),
(67, 'NB012_12/15', '2012', 1, 11, 1, 'Thinkpad Edge E420', 'อุปกรณ์หน้าจอ1', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 04:06:39', '2019-02-15 04:06:39'),
(68, 'NB012_12/19', '2016', 1, 11, 1, 'Thinkpad Edge E424', 'อุปกรณ์หน้าจอ5', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 04:06:39', '2019-02-15 04:06:39'),
(69, 'NB012_12/16', '2013', 2, 11, 1, 'Thinkpad Edge E421', 'อุปกรณ์หน้าจอ2', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 04:06:40', '2019-02-15 04:06:40'),
(90, 'NB012_12/15', '2012', 1, 11, 1, 'Thinkpad Edge E420', 'อุปกรณ์หน้าจอ', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 10:20:14', '2019-02-15 10:20:14'),
(91, 'NB012_12/15', '2012', 1, 11, 1, 'Thinkpad Edge E420', 'อุปกรณ์หน้าจอ1', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 10:41:55', '2019-02-15 10:41:55'),
(92, 'NB012_12/17', '2014', 1, 11, 1, 'Thinkpad Edge E422', 'อุปกรณ์หน้าจอ3', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 10:41:56', '2019-02-15 10:41:56'),
(93, 'NB012_12/16', '2013', 2, 11, 1, 'Thinkpad Edge E421', 'อุปกรณ์หน้าจอ2', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 10:41:56', '2019-02-15 10:41:56'),
(94, 'NB012_12/18', '2015', 2, 11, 1, 'Thinkpad Edge E423', 'อุปกรณ์หน้าจอ4', 'LNV-11412ET', 'R9ERK4T', 1, NULL, 'demo', '2019-02-15 10:41:57', '2019-02-15 10:41:57'),
(95, 'NB012_12/19', '2016', 1, 11, 2, 'Thinkpad Edge E424', 'อุปกรณ์หน้าจอ5', 'LNV-11412ET', 'R9ERK4T', 1, 'demo', 'demo', '2019-02-15 10:41:57', '2019-02-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `asset_book`
--

CREATE TABLE `asset_book` (
  `id` int(10) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `book_start` date DEFAULT NULL,
  `book_date` date DEFAULT NULL,
  `book_end_date` date DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_book`
--

INSERT INTO `asset_book` (`id`, `asset_id`, `user_id`, `book_start`, `book_date`, `book_end_date`, `status`, `created_at`, `updated_at`) VALUES
(27, 56, 2, NULL, '2018-12-16', '2018-12-28', 3, '2018-12-14 05:01:17', '2018-12-14 05:01:17'),
(29, 44, 4, NULL, '2018-12-17', '2018-12-28', 3, '2018-12-14 05:06:13', '2018-12-14 05:06:13'),
(48, 35, 25, NULL, '2018-12-25', '2018-12-31', 1, '2018-12-25 09:31:10', '2018-12-25 09:31:10'),
(50, 60, 26, NULL, '2018-12-28', '2018-12-31', 1, '2018-12-28 09:59:32', '2018-12-28 09:59:32'),
(51, 59, 26, NULL, '2018-12-28', '2018-12-31', 1, '2018-12-28 10:00:17', '2018-12-28 10:00:17'),
(55, 54, 25, NULL, '2019-01-01', '2019-01-02', 3, '2018-12-28 10:09:24', '2019-02-07 00:50:02'),
(57, 54, 26, NULL, '2019-01-15', '2019-01-31', 1, NULL, NULL),
(58, 54, 2, NULL, '2018-12-28', '2018-12-31', 3, NULL, NULL),
(61, 61, 26, NULL, '2019-01-14', '2019-01-30', 1, '2019-01-14 03:43:48', '2019-01-14 03:43:48'),
(62, 61, 44, NULL, '2019-02-12', '2019-02-28', 1, '2019-01-14 06:19:44', '2019-01-14 06:19:44'),
(63, 59, 44, NULL, '2019-01-24', '2019-01-31', 1, '2019-01-14 06:29:00', '2019-01-14 06:29:00'),
(64, 61, 44, NULL, '2019-02-01', '2019-02-04', 1, '2019-01-15 02:19:49', '2019-01-15 02:19:49'),
(65, 61, 25, NULL, '2019-03-06', '2019-03-20', 1, '2019-01-30 06:32:06', '2019-01-30 06:32:06'),
(67, 61, 26, NULL, '2019-02-13', '2019-02-20', 1, '2019-01-31 10:09:50', '2019-01-31 10:09:50'),
(68, 60, 31, NULL, '2019-02-13', '2019-02-20', 1, '2019-01-31 10:10:23', '2019-01-31 10:10:23'),
(69, 61, 25, NULL, '2019-03-24', '2019-03-29', 1, '2019-01-31 10:12:06', '2019-01-31 10:12:06'),
(70, 61, 30, NULL, '2019-04-09', '2019-04-12', 1, '2019-01-31 10:13:56', '2019-01-31 10:13:56'),
(71, 60, 25, NULL, '2019-02-22', '2019-02-26', 1, '2019-01-31 10:16:11', '2019-01-31 10:16:11'),
(72, 60, 2, NULL, '2019-03-01', '2019-03-08', 1, '2019-01-31 10:17:38', '2019-01-31 10:17:38'),
(73, 60, 4, NULL, '2019-04-10', '2019-04-17', 1, '2019-01-31 10:18:08', '2019-01-31 10:18:08'),
(74, 60, 46, NULL, '2019-03-13', '2019-03-20', 1, '2019-02-07 00:27:50', '2019-02-07 00:27:50'),
(75, 54, 25, NULL, '2019-02-14', '2019-02-20', 1, '2019-02-07 00:45:45', '2019-02-07 00:45:45'),
(76, 54, 2, NULL, '2019-02-06', '2019-02-12', 1, '2019-02-07 00:46:51', '2019-02-07 00:58:32'),
(77, 59, 26, NULL, '2019-02-07', '2019-02-08', 2, '2019-02-07 00:52:21', '2019-02-07 00:52:36'),
(78, 44, 46, NULL, '2019-02-07', '2019-02-13', 1, '2019-02-07 01:03:56', '2019-02-07 01:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `asset_categorie`
--

CREATE TABLE `asset_categorie` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `computer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset_categorie`
--

INSERT INTO `asset_categorie` (`id`, `value`, `text`, `computer`) VALUES
(1, 'Notebook', 'Notebook', 1),
(2, 'Monitor', 'Monitor', 0);

-- --------------------------------------------------------

--
-- Table structure for table `asset_users`
--

CREATE TABLE `asset_users` (
  `asset_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset_users`
--

INSERT INTO `asset_users` (`asset_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(35, 4, 0, '2018-11-15 00:00:00', NULL),
(35, 26, 1, '2019-01-29 17:00:00', '2019-01-29 17:00:00'),
(39, 26, 1, '2018-12-24 00:00:00', NULL),
(40, 2, 0, '2018-12-24 00:00:00', '2018-12-24 00:00:00'),
(40, 26, 1, '2019-01-14 00:00:00', NULL),
(40, 44, 0, '2018-11-26 00:00:00', NULL),
(42, 2, 1, '2019-01-02 00:00:00', '2019-01-02 00:00:00'),
(42, 4, 0, '2018-12-24 00:00:00', '2018-12-24 00:00:00'),
(54, 2, 0, '2018-11-22 00:00:00', NULL),
(54, 30, 0, '2018-11-22 00:00:00', NULL),
(57, 4, 0, '2018-11-14 00:00:00', NULL),
(57, 30, 1, '2019-01-29 17:00:00', NULL),
(57, 32, 0, '2019-01-29 17:00:00', NULL),
(58, 2, 0, '2019-01-29 17:00:00', '2019-01-29 17:00:00'),
(58, 4, 0, '2019-01-29 17:00:00', '2019-01-31 17:00:00'),
(58, 25, 0, '2018-11-14 00:00:00', NULL),
(58, 26, 1, '2019-01-29 17:00:00', '2019-02-14 17:00:00'),
(58, 31, 0, '2019-01-29 17:00:00', '2019-01-29 17:00:00'),
(58, 43, 0, '2019-01-29 17:00:00', NULL),
(58, 44, 0, '2018-11-26 00:00:00', '2019-01-14 00:00:00'),
(59, 32, 0, '2018-11-14 00:00:00', NULL),
(60, 2, 0, '2018-12-25 00:00:00', NULL),
(60, 4, 0, '2018-11-20 00:00:00', NULL),
(60, 25, 0, '2018-11-22 00:00:00', NULL),
(60, 26, 0, '2018-11-14 00:00:00', '2018-11-26 00:00:00'),
(60, 31, 0, '2018-11-20 00:00:00', NULL),
(61, 2, 0, '2018-11-14 00:00:00', '2018-12-24 00:00:00'),
(61, 4, 0, '2018-11-15 00:00:00', '2019-02-12 17:00:00'),
(61, 25, 1, '2018-11-15 00:00:00', '2019-02-12 17:00:00'),
(61, 26, 0, '2018-11-16 00:00:00', '2019-01-02 00:00:00'),
(61, 32, 0, '2018-11-26 00:00:00', NULL),
(61, 46, 0, '2019-02-06 17:00:00', NULL),
(65, 25, 1, '2019-02-14 17:00:00', NULL),
(65, 26, 0, '2019-02-14 17:00:00', NULL),
(95, 26, 1, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(95, 30, 0, '2019-02-14 17:00:00', NULL),
(95, 31, 0, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(95, 32, 0, '2019-02-14 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `computer`
--

CREATE TABLE `computer` (
  `id` int(11) NOT NULL,
  `cpu_name` varchar(45) DEFAULT NULL,
  `harddisk_type` varchar(10) DEFAULT NULL,
  `harddisk_qty` int(11) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `computer`
--

INSERT INTO `computer` (`id`, `cpu_name`, `harddisk_type`, `harddisk_qty`, `ram`, `display`, `asset_id`, `updated_at`, `created_at`) VALUES
(19, 'Intel Core i3', 'HDD', 512, 8, 14, 39, '2018-10-16 07:00:13', '2018-10-16 07:00:13'),
(20, 'Intel Core i3', 'HDD', 512, 8, 14, 40, '2018-10-16 07:02:42', '2018-10-16 07:02:42'),
(22, NULL, NULL, NULL, NULL, NULL, 42, '2018-10-17 07:15:49', '2018-10-16 07:05:06'),
(24, 'Intel Core i3', 'HDD', 512, 8, 14, 44, '2018-10-18 04:01:48', '2018-10-18 04:01:48'),
(34, NULL, 'HDD', 512, 8, 14, 54, '2018-10-18 04:20:12', '2018-10-18 04:20:12'),
(36, 'Intel Core i3', 'HDD', 512, 8, 15, 56, '2018-10-18 04:21:49', '2018-10-18 04:21:49'),
(37, 'Intel Core i3', 'HDD', 512, 8, 15, 57, '2018-11-05 02:03:40', '2018-11-05 02:03:40'),
(38, 'Intel Core i5', 'HDD', 512, 8, 14, 58, '2018-11-06 05:00:00', '2018-11-06 05:00:00'),
(39, NULL, NULL, NULL, NULL, NULL, 59, '2018-11-06 06:07:21', '2018-11-06 06:07:21'),
(40, NULL, NULL, NULL, NULL, NULL, 60, '2018-11-06 06:08:28', '2018-11-06 06:08:28'),
(41, NULL, NULL, NULL, NULL, NULL, 62, '2019-02-15 08:46:40', '2019-02-15 08:46:40'),
(43, NULL, NULL, NULL, NULL, NULL, 64, '2019-02-15 08:48:45', '2019-02-15 08:48:45'),
(44, NULL, NULL, NULL, NULL, NULL, 65, '2019-02-15 08:50:53', '2019-02-15 08:50:53'),
(45, NULL, NULL, NULL, NULL, NULL, 66, '2019-02-15 11:05:06', '2019-02-15 11:05:06'),
(46, NULL, NULL, NULL, NULL, NULL, 67, '2019-02-15 11:06:39', '2019-02-15 11:06:39'),
(47, NULL, NULL, NULL, NULL, NULL, 68, '2019-02-15 11:06:39', '2019-02-15 11:06:39'),
(48, NULL, NULL, NULL, NULL, NULL, 69, '2019-02-15 11:06:40', '2019-02-15 11:06:40'),
(69, NULL, NULL, NULL, NULL, NULL, 90, '2019-02-15 17:20:14', '2019-02-15 17:20:14'),
(70, NULL, NULL, NULL, NULL, NULL, 91, '2019-02-15 17:41:55', '2019-02-15 17:41:55'),
(71, NULL, NULL, NULL, NULL, NULL, 92, '2019-02-15 17:41:56', '2019-02-15 17:41:56'),
(72, NULL, NULL, NULL, NULL, NULL, 93, '2019-02-15 17:41:56', '2019-02-15 17:41:56'),
(73, NULL, NULL, NULL, NULL, NULL, 94, '2019-02-15 17:41:57', '2019-02-15 17:41:57'),
(74, NULL, NULL, NULL, NULL, NULL, 95, '2019-02-15 17:41:57', '2019-02-15 17:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `value`, `text`) VALUES
(1, 'IS-CNX', 'IS-CNX'),
(2, 'PHP', 'PHP'),
(3, 'Mobile', 'Mobile'),
(4, 'FTL', 'FTL'),
(5, 'BI', 'BI'),
(6, 'Test', 'Test'),
(7, 'Account', 'Account'),
(8, 'AEX', 'AEX'),
(9, 'Designer', 'Designer'),
(10, 'PMO', 'PMO'),
(11, 'Admin', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(101, '2014_10_12_000000_create_users_table', 1),
(102, '2014_10_12_100000_create_password_resets_table', 1),
(103, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(104, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(105, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(106, '2016_06_01_000004_create_oauth_clients_table', 1),
(107, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(108, '2018_10_08_00_asset_categorie', 1),
(109, '2018_10_08_00_department', 1),
(110, '2018_10_08_032658_asset', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Available'),
(2, 'Usage'),
(3, 'Booking'),
(4, 'Retired Asset'),
(5, 'Usage By Booking');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `name`, `department_id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `user_type`) VALUES
(2, 'thanapon666', 'prunktan', 'tiekungsos', 1, 'beeze005@hotmail.com', NULL, '$2y$10$Jf6gzVVc4LaaAntC7c4a6.VyefW6X64NKCnEzpmcNTYKjQ3g3dChe', NULL, '2018-10-11 04:45:02', '2019-01-02 08:46:09', 'admin'),
(4, 'thanapon', 'prunktan', 'tiekungsos', 1, 'beeze001@gmail.com', NULL, '$2y$10$X1vZy4wd2FExIpzWd9/kx.Aedd/0fOpifVo8Q5eVoF8CbgAGUxTBa', NULL, '2018-10-11 04:59:55', '2018-11-15 02:33:39', 'admin'),
(25, 'ธนพล', 'พฤกทาน', 'demo', 4, 'beeze008@hotmail.com', NULL, '$2y$10$9vrK1wS1VdUCn/ySFUuuwu1O/XRgV.we9pnatSPhncgcdQ.NM783e', NULL, '2018-11-14 07:22:53', '2018-11-14 07:22:53', 'employee'),
(26, 'Thanapon', 'Prunktan', 'demo', 2, 'beeze009@gmail.com', NULL, '$2y$10$d/DAMFznt05g16ZRH2eXiOi07d.eXf/dlnjEaADcdR937dVW6w9Xu', NULL, '2018-11-14 07:25:38', '2019-01-15 04:09:53', 'employee'),
(30, 'Thanapon', 'Prunktan', 'demo', 2, 'beeze000@gmail.com', NULL, '$2y$10$3g/So95MDmj/cuXQR/9bj.ihwtu2kQ306Rm8GtdiNz1YCpD4zm9/i', NULL, '2018-11-14 07:35:08', '2018-11-14 07:35:08', 'admin'),
(31, 'Thanapon', 'Prunktan', 'demo', 2, 'beeze0070@gmail.com', NULL, '$2y$10$RLFDfx8oCGF7X2qglINYE.zp0VFRkpSXB.SiXK9C77Y2nrlOdmIaO', NULL, '2018-11-14 07:37:11', '2018-11-14 07:37:11', 'admin'),
(32, 'tiekung', 'sos', 'demo', 2, 'beeze000@hotmail.com', NULL, '$2y$10$A6mrH9xjyFD/jtJVgasdk.znZSP8fXXaG0qSpT3E8hgP7VI2Z6K2q', NULL, '2018-11-14 07:40:41', '2018-11-14 07:40:41', 'admin'),
(41, 'th', 'pppp', 'tiekungsosaaaa', 1, 'beeze007@hotmail.com', NULL, '$2y$10$aDzk5.Nj.oXEzS1GjV0KMOqHGJEXOiivpXIA/nkOSes5rQa3mrZEO', NULL, '2018-12-24 10:57:16', '2018-12-24 10:57:16', 'admin'),
(42, 'th', 'pppp', 'tiekungsosaaaa', 1, 'beeze0073@hotmail.com', NULL, '$2y$10$.6s5PHvnkm65sLu5ZZqWFuetlFQG0fh3McOko41WIkPq0OJe.RvIS', NULL, '2018-12-24 10:57:40', '2018-12-24 10:57:40', 'admin'),
(43, 'th', 'pppp', 'tiekungsosaaaa', 1, 'beeze0073d@hotmail.com', NULL, '$2y$10$SapaxCT0pMgffpueH1JtkOCYa5Qncs28cQ.JltzkzFi2XlgWwyeBS', NULL, '2018-12-24 10:59:20', '2018-12-24 10:59:20', 'admin'),
(44, 'tiekung', 'sos', 'pos', 6, 'beeze009@hotmail.com', NULL, '$2y$10$ptBftV3MLFN6tCC0EMyeBeEAsoCzWQUYD7qA397kyS25IP2oObW3q', NULL, '2018-12-24 11:21:11', '2019-01-15 04:18:32', 'employee'),
(45, 'Thanapon', 'Prunktan', 'demo', 9, 'beeze010@hotmail.com', NULL, '$2y$10$6XG86kql75sewVYeKDwRXuz/WO5Ls4HMhzCns3otAsV/GzQ4c8DDi', NULL, '2019-01-11 06:11:51', '2019-01-11 06:11:51', 'admin'),
(46, 'mayriday', 'sos', 'may', 9, 'beeze002@gmail.com', NULL, '$2y$10$BPTnpD13OLdoSYfQg0P0Wuj2VF3w7BInDYAFqGKPQEZKF.tlY9C9q', NULL, '2019-01-31 09:16:59', '2019-01-31 09:16:59', 'employee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_categorie_id_foreign` (`categorie_id`),
  ADD KEY `asset_department_id_foreign` (`department_id`),
  ADD KEY `asset_status_id_foreign` (`status_id`);

--
-- Indexes for table `asset_book`
--
ALTER TABLE `asset_book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asset_has_users_asset1_idx` (`asset_id`),
  ADD KEY `fk_asset_has_users_users1_idx` (`user_id`);

--
-- Indexes for table `asset_categorie`
--
ALTER TABLE `asset_categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_users`
--
ALTER TABLE `asset_users`
  ADD PRIMARY KEY (`asset_id`,`user_id`),
  ADD KEY `fk_asset_has_users_users1_idx` (`user_id`),
  ADD KEY `fk_asset_has_users_asset1_idx` (`asset_id`);

--
-- Indexes for table `computer`
--
ALTER TABLE `computer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `computer_asset_id_foreign` (`asset_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `fk_users_1_idx` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset`
--
ALTER TABLE `asset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `asset_book`
--
ALTER TABLE `asset_book`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `asset_categorie`
--
ALTER TABLE `asset_categorie`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `computer`
--
ALTER TABLE `computer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asset`
--
ALTER TABLE `asset`
  ADD CONSTRAINT `asset_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `asset_categorie` (`id`),
  ADD CONSTRAINT `asset_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `asset_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `asset_users`
--
ALTER TABLE `asset_users`
  ADD CONSTRAINT `fk_asset_has_users_asset1` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_asset_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `computer`
--
ALTER TABLE `computer`
  ADD CONSTRAINT `computer_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
