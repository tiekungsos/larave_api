-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: vue
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.9-MariaDB-1:10.3.9+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categorie_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `asset_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `part_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_status` int(2) NOT NULL,
  `asset_user` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_categorie_id_foreign` (`categorie_id`),
  KEY `asset_department_id_foreign` (`department_id`),
  KEY `asset_status_id_foreign` (`status_id`),
  CONSTRAINT `asset_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `asset_categorie` (`id`),
  CONSTRAINT `asset_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `asset_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (35,'NB012_12/15','2012',2,8,5,'Thinkpad Edge E420','อุปกรณ์หน้าจอ','R9ERK4T','LNV-11412ET',0,'tiekungsos','สวัสดี','2018-10-09 09:33:56','2018-12-25 10:27:13'),(39,'NB012_12/15','2012',1,1,2,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch','R9ERK4T','LNV-11412ET',0,'demo','uuuu','2018-10-16 07:00:13','2018-12-24 00:00:00'),(40,'NB012_12/15','2012',1,2,2,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch','R9ERK4T','LNV-11412ET',0,'demo','iiiii','2018-10-16 07:02:42','2019-01-14 00:00:00'),(42,'NB012_12/15','2012',1,2,2,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch','R9ERK4T','LNV-11412ET',0,'tiekungsos','ttt','2018-10-16 07:05:06','2019-01-02 00:00:00'),(44,'NB012_12/15','2012',1,2,3,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch','R9ERK4T','LNV-11412ET',0,NULL,'tttt','2018-10-18 04:01:48','2018-12-14 05:06:13'),(54,'NB012_12/15','2018',2,4,3,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 13 inch','R9ERK4T','oooooo',0,NULL,'ooooo','2018-10-18 04:20:12','2019-01-02 05:10:09'),(56,'NB012_12/15','2012',1,1,5,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 15 inch','R9ERK4T','LNV-11412ET',0,'tiekungsos','uuu','2018-10-18 04:21:49','2018-12-25 10:27:12'),(57,'NB012_12/15','2012',1,1,2,'Thinkpad Edge E420','CPU Intel Core i3, Harddisk HDD 512GB ,RAM 8GB ,Display 15 inch','R9ERK4T','LNV-11412ET',1,'tiekungsos','tertert','2018-11-05 02:03:40','2018-11-14 00:00:00'),(58,'NB012_12/15','2012',1,3,2,'Thinkpad Edge E420','CPU Intel Core i5, Harddisk HDD 512GB ,RAM 8GB ,Display 14 inch','R9ERK4T','LNV-11412ET',1,'pos','ooooo','2018-11-06 05:00:00','2019-01-14 00:00:00'),(59,'NB012_12/15','2012',2,2,3,'Thinkpad Edge E420','อุปกรณ์หน้าจอ','R9ERK4T','LNV-11412ET',1,NULL,'dasdas','2018-11-06 06:07:21','2018-12-28 10:00:17'),(60,'NB012_12/15','2015',2,2,5,'Thinkpad Edge E420','addas','R9ERK4T','LNV-11412ET',1,NULL,'asdsad','2018-11-06 06:08:28','2018-12-28 10:00:04'),(61,'NB012_12/1','2013',1,3,3,'Thinkpad Edge E420','CPU Intel Core i5, Harddisk HDD 512GB ,RAM 8GB ,Display 15 inch','R10ERK4T','LNV-11412ET',1,NULL,'ปุ่มพัง22','2018-11-06 06:13:15','2019-01-14 03:43:48');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_book`
--

DROP TABLE IF EXISTS `asset_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_book` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `book_start` date DEFAULT NULL,
  `book_date` date DEFAULT NULL,
  `book_end_date` date DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_asset_has_users_asset1_idx` (`asset_id`),
  KEY `fk_asset_has_users_users1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_book`
--

LOCK TABLES `asset_book` WRITE;
/*!40000 ALTER TABLE `asset_book` DISABLE KEYS */;
INSERT INTO `asset_book` VALUES (27,56,2,NULL,'2018-12-16','2018-12-28',3,'2018-12-14 05:01:17','2018-12-14 05:01:17'),(29,44,4,NULL,'2018-12-17','2018-12-28',3,'2018-12-14 05:06:13','2018-12-14 05:06:13'),(48,35,25,NULL,'2018-12-25','2018-12-31',1,'2018-12-25 09:31:10','2018-12-25 09:31:10'),(50,60,26,NULL,'2018-12-28','2018-12-31',1,'2018-12-28 09:59:32','2018-12-28 09:59:32'),(51,59,26,NULL,'2018-12-28','2018-12-31',1,'2018-12-28 10:00:17','2018-12-28 10:00:17'),(55,54,25,NULL,'2019-01-01','2019-01-02',4,'2018-12-28 10:09:24','2019-01-02 07:00:02'),(57,54,26,NULL,'2019-01-15','2019-01-31',1,NULL,NULL),(58,54,2,NULL,'2018-12-28','2018-12-31',3,NULL,NULL),(61,61,26,NULL,'2019-01-14','2019-01-30',1,'2019-01-14 03:43:48','2019-01-14 03:43:48'),(62,61,44,NULL,'2019-02-12','2019-02-28',1,'2019-01-14 06:19:44','2019-01-14 06:19:44'),(63,59,44,NULL,'2019-01-24','2019-01-31',1,'2019-01-14 06:29:00','2019-01-14 06:29:00'),(64,61,44,NULL,'2019-02-01','2019-02-04',1,'2019-01-15 02:19:49','2019-01-15 02:19:49');
/*!40000 ALTER TABLE `asset_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_categorie`
--

DROP TABLE IF EXISTS `asset_categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_categorie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `computer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_categorie`
--

LOCK TABLES `asset_categorie` WRITE;
/*!40000 ALTER TABLE `asset_categorie` DISABLE KEYS */;
INSERT INTO `asset_categorie` VALUES (1,'Notebook','Notebook',1),(2,'Monitor','Monitor',0);
/*!40000 ALTER TABLE `asset_categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_users`
--

DROP TABLE IF EXISTS `asset_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_users` (
  `asset_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asset_id`,`user_id`),
  KEY `fk_asset_has_users_users1_idx` (`user_id`),
  KEY `fk_asset_has_users_asset1_idx` (`asset_id`),
  CONSTRAINT `fk_asset_has_users_asset1` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asset_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_users`
--

LOCK TABLES `asset_users` WRITE;
/*!40000 ALTER TABLE `asset_users` DISABLE KEYS */;
INSERT INTO `asset_users` VALUES (35,4,0,'2018-11-15 00:00:00',NULL),(39,26,1,'2018-12-24 00:00:00',NULL),(40,2,0,'2018-12-24 00:00:00','2018-12-24 00:00:00'),(40,26,1,'2019-01-14 00:00:00',NULL),(40,44,0,'2018-11-26 00:00:00',NULL),(42,2,1,'2019-01-02 00:00:00','2019-01-02 00:00:00'),(42,4,0,'2018-12-24 00:00:00','2018-12-24 00:00:00'),(54,2,0,'2018-11-22 00:00:00',NULL),(54,30,0,'2018-11-22 00:00:00',NULL),(57,4,1,'2018-11-14 00:00:00',NULL),(58,25,0,'2018-11-14 00:00:00',NULL),(58,44,1,'2018-11-26 00:00:00','2019-01-14 00:00:00'),(59,32,0,'2018-11-14 00:00:00',NULL),(60,2,0,'2018-12-25 00:00:00',NULL),(60,4,0,'2018-11-20 00:00:00',NULL),(60,25,0,'2018-11-22 00:00:00',NULL),(60,26,0,'2018-11-14 00:00:00','2018-11-26 00:00:00'),(60,31,0,'2018-11-20 00:00:00',NULL),(61,2,0,'2018-11-14 00:00:00','2018-12-24 00:00:00'),(61,4,0,'2018-11-15 00:00:00','2019-01-02 00:00:00'),(61,25,0,'2018-11-15 00:00:00','2018-12-24 00:00:00'),(61,26,0,'2018-11-16 00:00:00','2019-01-02 00:00:00'),(61,32,0,'2018-11-26 00:00:00',NULL);
/*!40000 ALTER TABLE `asset_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computer`
--

DROP TABLE IF EXISTS `computer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpu_name` varchar(45) DEFAULT NULL,
  `harddisk_type` varchar(10) DEFAULT NULL,
  `harddisk_qty` int(11) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  `asset_id` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `computer_asset_id_foreign` (`asset_id`),
  CONSTRAINT `computer_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computer`
--

LOCK TABLES `computer` WRITE;
/*!40000 ALTER TABLE `computer` DISABLE KEYS */;
INSERT INTO `computer` VALUES (19,'Intel Core i3','HDD',512,8,14,39,'2018-10-16 07:00:13','2018-10-16 07:00:13'),(20,'Intel Core i3','HDD',512,8,14,40,'2018-10-16 07:02:42','2018-10-16 07:02:42'),(22,NULL,NULL,NULL,NULL,NULL,42,'2018-10-17 07:15:49','2018-10-16 07:05:06'),(24,'Intel Core i3','HDD',512,8,14,44,'2018-10-18 04:01:48','2018-10-18 04:01:48'),(34,NULL,'HDD',512,8,14,54,'2018-10-18 04:20:12','2018-10-18 04:20:12'),(36,'Intel Core i3','HDD',512,8,15,56,'2018-10-18 04:21:49','2018-10-18 04:21:49'),(37,'Intel Core i3','HDD',512,8,15,57,'2018-11-05 02:03:40','2018-11-05 02:03:40'),(38,'Intel Core i5','HDD',512,8,14,58,'2018-11-06 05:00:00','2018-11-06 05:00:00'),(39,NULL,NULL,NULL,NULL,NULL,59,'2018-11-06 06:07:21','2018-11-06 06:07:21'),(40,NULL,NULL,NULL,NULL,NULL,60,'2018-11-06 06:08:28','2018-11-06 06:08:28');
/*!40000 ALTER TABLE `computer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'IS-CNX','IS-CNX'),(2,'PHP','PHP'),(3,'Mobile','Mobile'),(4,'FTL','FTL'),(5,'BI','BI'),(6,'Test','Test'),(7,'Account','Account'),(8,'AEX','AEX'),(9,'Designer','Designer'),(10,'PMO','PMO');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (101,'2014_10_12_000000_create_users_table',1),(102,'2014_10_12_100000_create_password_resets_table',1),(103,'2016_06_01_000001_create_oauth_auth_codes_table',1),(104,'2016_06_01_000002_create_oauth_access_tokens_table',1),(105,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(106,'2016_06_01_000004_create_oauth_clients_table',1),(107,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(108,'2018_10_08_00_asset_categorie',1),(109,'2018_10_08_00_department',1),(110,'2018_10_08_032658_asset',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Available'),(2,'Usage'),(3,'Booking'),(4,'Retired Asset'),(5,'Usage By Booking');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `fk_users_1_idx` (`department_id`),
  CONSTRAINT `fk_users_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'thanapon666','prunktan','tiekungsos',1,'beeze005@hotmail.com',NULL,'$2y$10$Jf6gzVVc4LaaAntC7c4a6.VyefW6X64NKCnEzpmcNTYKjQ3g3dChe',NULL,'2018-10-11 04:45:02','2019-01-02 08:46:09','admin'),(4,'thanapon','prunktan','tiekungsos',1,'beeze001@gmail.com',NULL,'$2y$10$X1vZy4wd2FExIpzWd9/kx.Aedd/0fOpifVo8Q5eVoF8CbgAGUxTBa',NULL,'2018-10-11 04:59:55','2018-11-15 02:33:39','admin'),(25,'ธนพล','พฤกทาน','demo',4,'beeze008@hotmail.com',NULL,'$2y$10$9vrK1wS1VdUCn/ySFUuuwu1O/XRgV.we9pnatSPhncgcdQ.NM783e',NULL,'2018-11-14 07:22:53','2018-11-14 07:22:53','employee'),(26,'Thanapon','Prunktan','demo',2,'beeze009@gmail.com',NULL,'$2y$10$d/DAMFznt05g16ZRH2eXiOi07d.eXf/dlnjEaADcdR937dVW6w9Xu',NULL,'2018-11-14 07:25:38','2019-01-15 04:09:53','employee'),(30,'Thanapon','Prunktan','demo',2,'beeze000@gmail.com',NULL,'$2y$10$3g/So95MDmj/cuXQR/9bj.ihwtu2kQ306Rm8GtdiNz1YCpD4zm9/i',NULL,'2018-11-14 07:35:08','2018-11-14 07:35:08','admin'),(31,'Thanapon','Prunktan','demo',2,'beeze0070@gmail.com',NULL,'$2y$10$RLFDfx8oCGF7X2qglINYE.zp0VFRkpSXB.SiXK9C77Y2nrlOdmIaO',NULL,'2018-11-14 07:37:11','2018-11-14 07:37:11','admin'),(32,'tiekung','sos','demo',2,'beeze000@hotmail.com',NULL,'$2y$10$A6mrH9xjyFD/jtJVgasdk.znZSP8fXXaG0qSpT3E8hgP7VI2Z6K2q',NULL,'2018-11-14 07:40:41','2018-11-14 07:40:41','admin'),(41,'th','pppp','tiekungsosaaaa',1,'beeze007@hotmail.com',NULL,'$2y$10$aDzk5.Nj.oXEzS1GjV0KMOqHGJEXOiivpXIA/nkOSes5rQa3mrZEO',NULL,'2018-12-24 10:57:16','2018-12-24 10:57:16','admin'),(42,'th','pppp','tiekungsosaaaa',1,'beeze0073@hotmail.com',NULL,'$2y$10$.6s5PHvnkm65sLu5ZZqWFuetlFQG0fh3McOko41WIkPq0OJe.RvIS',NULL,'2018-12-24 10:57:40','2018-12-24 10:57:40','admin'),(43,'th','pppp','tiekungsosaaaa',1,'beeze0073d@hotmail.com',NULL,'$2y$10$SapaxCT0pMgffpueH1JtkOCYa5Qncs28cQ.JltzkzFi2XlgWwyeBS',NULL,'2018-12-24 10:59:20','2018-12-24 10:59:20','admin'),(44,'tiekung','sos','pos',6,'beeze009@hotmail.com',NULL,'$2y$10$ptBftV3MLFN6tCC0EMyeBeEAsoCzWQUYD7qA397kyS25IP2oObW3q',NULL,'2018-12-24 11:21:11','2019-01-15 04:18:32','employee'),(45,'Thanapon','Prunktan','demo',9,'beeze010@hotmail.com',NULL,'$2y$10$6XG86kql75sewVYeKDwRXuz/WO5Ls4HMhzCns3otAsV/GzQ4c8DDi',NULL,'2019-01-11 06:11:51','2019-01-11 06:11:51','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-29  9:17:20
